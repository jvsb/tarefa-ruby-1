def input(arrays)
    #declarando variaveis
    soma = 0
    mul = 1
    #criando um loop
    arrays.each do |array|
      array.each do |num|
        #realizando a conta
        soma = num + soma
        mul = num * mul
      end
    end
    puts("Soma: #{soma} ")
    puts("Multiplicação: #{mul} ")
  end
  questao = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
  
  input(questao)